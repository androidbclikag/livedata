package com.example.livedata

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.BindingAdapter

object DataBindingComponents {
    @JvmStatic
    @BindingAdapter("setVisibility")
    fun View.setVisibility(visibility: Boolean) {
         if (visibility)
            VISIBLE
        else
            GONE

    }
}