package com.example.livedata

import android.os.Handler

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DataHolderViewModel : ViewModel() {

    val progressBarVisibility = MutableLiveData<Boolean>()
    var responseResult = MutableLiveData<String>()

    fun logIn(email: String, password: String) {

        progressBarVisibility.value = true
        Handler().postDelayed(
            {
                progressBarVisibility.value = false

                if (email.isNotEmpty() && password.isNotEmpty())
                    responseResult.value = " result is Successful"
                else responseResult.value = "Failure"

            },
            2000
        )


    }


}